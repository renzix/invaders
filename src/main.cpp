// SDL2 Hello, World!
// This should display a white screen for 2 seconds
// compile with: clang++ main.cpp -o hello_sdl2 -lSDL2
// run with: ./hello_sdl2

#include <stdio.h>
#include <unistd.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_rect.h>

#include "player.h"
#include "alien.h"

SDL_Window* window = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture * texture = NULL;

const int SCREEN_WIDTH  = 640;
const int SCREEN_HEIGHT = 480;

Entity* list_of_entities[100];

bool running = true;

int main(int argc, char* args[]) {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
    return 1;
  }
  window = SDL_CreateWindow(
      "Invaders",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      SCREEN_WIDTH, SCREEN_HEIGHT,
      SDL_WINDOW_SHOWN
    );
  if (window == NULL) {
    fprintf(stderr, "could not create window: %s\n", SDL_GetError());
    return 1;
  }
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  SDL_SetRenderDrawColor(renderer, 0,0,0,255);
  SDL_RenderClear(renderer);

  SDL_Surface *cr = SDL_CreateRGBSurface(NULL, 1000, 1000, 8, 0, 0, 0, 255);

  texture = SDL_CreateTextureFromSurface(renderer, cr);


  // init game
  Player player;
  bool right_pressed = false;
  bool left_pressed = false;

  size_t amount_of_aliens_column = 10;
  size_t amount_of_aliens_row    = 7;
  size_t amount_of_aliens = amount_of_aliens_row*amount_of_aliens_column;
  Alien* aliens[amount_of_aliens];
  constexpr int x_space = SCREEN_WIDTH/10;
  constexpr int y_space = SCREEN_HEIGHT/16;
  for (int i = 0; i < amount_of_aliens_column; i++)
  {
      for (int j = 0; j < amount_of_aliens_row; j++)
      {
          aliens[i+(j*i)] = new Alien(i*x_space, (j)*y_space, j%2);
      }
  }

  // GAME LOOP
  SDL_Event event;
  while (running==true)
  {
      while(SDL_PollEvent(&event))
      {
          switch (event.type)
          {
              case SDL_KEYDOWN:
              {
                   switch (event.key.keysym.sym)
                   {
                       case SDLK_LEFT:
                       {
                           left_pressed = true;
                           printf("right pressed: 0, left_pressed %i\n", left_pressed);
                           player.speed = -1;
                           break;
                       }
                       case SDLK_RIGHT:
                       {
                           right_pressed = true;
                           printf("right pressed: %i, left_pressed 0\n", right_pressed);
                           player.speed = 1;
                           break;
                       }
                       case SDLK_SPACE:
                       {
                           player.shoot();
                           break;
                       }
                       case SDLK_a:
                       {
                           printf("a\n");
                           break;
                       }
                       case SDLK_q:
                       {
                           printf("exiting...");
                           running = false;
                           break;
                       }

                       default:
                       {
                           printf("Pressed key %u\n", event.key.keysym.sym);
                           break;
                       }
                   }
                  break;
              }
              case SDL_KEYUP:
              {
                  switch (event.key.keysym.sym)
                  {
                      case SDLK_RIGHT:
                      {
                          printf("right pressed: 0, left_pressed %i\n", left_pressed);
                          right_pressed = false;
                          if (left_pressed == false)
                              player.speed = 0;
                          else
                              player.speed = -1;
                          break;
                      }
                      case SDLK_LEFT:
                      {
                          printf("right pressed: %i, left_pressed 0\n", right_pressed);
                          left_pressed = false;
                          if (right_pressed == false)
                              player.speed = 0;
                          else
                              player.speed = 1;
                          break;
                      }
                      default:
                      {
                          printf("Released key %u\n", event.key.keysym.sym);
                          break;
                      }
                  }
              }
              default:
              {
                  printf("unhandled event type: %i\n", event.type);
              }
          }
      }
      for (int i=0; i<100; i++)
      {
          if (list_of_entities[i]!=NULL)
          {
              list_of_entities[i]->move();
          }
      }

      SDL_RenderClear(renderer);
      for (int i=0; i<100; i++)
      {
          if (list_of_entities[i]!=NULL)
          {
              list_of_entities[i]->draw();
          }
      }
      SDL_SetRenderDrawColor(renderer, 0,0,0,255);
      SDL_RenderPresent(renderer);
      usleep(2000);
  }

  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}

