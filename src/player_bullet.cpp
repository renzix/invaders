#include "player_bullet.h"

PlayerBullet::PlayerBullet(uint16_t x, uint16_t y) : Entity()
{
    name="player-bullet";
    this->x = x;
    this->y = y;
    printf("creating player bullet\n");
}

void PlayerBullet::draw()
{
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = width;
    rect.h = height;
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawRect(renderer, &rect);
    SDL_RenderFillRect(renderer, &rect);
}

void PlayerBullet::move()
{
    y-=1;
    // off the screen
    if (y<=0)
    {
        delete this;
    }
    else
    {
        // loop through aliens
        for (int i = 0; i < 100; i++)
        {
            if (list_of_entities[i]!=NULL)
            {
                if(strcmp(list_of_entities[i]->name, "alien")==0)
                {
                    Alien* a = (Alien*)(list_of_entities[i]);

                    if (((a->y+a->width) > (y)) && ((a->x) < x) && ((a->x+a->width) > x))
                    {
                        a->~Alien(); // i dont know why i cant just delete this
                        delete this;
                        return;
                    }
                }
            }
        }
    }
}

PlayerBullet::~PlayerBullet()
{
    printf("deleting player bullet\n");
}
