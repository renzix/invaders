#include "entity.h"

Entity::Entity()
{
    for (int i = 0; i<100; i++)
    {
        if (list_of_entities[i]==NULL)
        {
            list_of_entities[i] = this;
            id = i;
            printf("creating entity, id %u\n", id);
            return;
        }
    }
    exit(1);
}

Entity::~Entity()
{
    list_of_entities[id] = NULL;
    printf("deleting entity, id %u\n", id);
}
