#include "player.h"

void Player::draw()
{
    SDL_Rect body;
    body.x = x;
    body.y = y;
    body.w = width;
    body.h = height;
    SDL_Rect tip;
    tip.x = (body.x+body.w/2)-body.w*0.2;
    tip.y = body.y;
    tip.w = body.w*0.4;
    tip.h = -body.h/4;
    SDL_Rect tiptip;
    tiptip.x = (body.x+body.w/2)-body.w*0.05;
    tiptip.y = body.y+tip.h;
    tiptip.w = body.w*0.1;
    tiptip.h = -body.h/4;
    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
    SDL_RenderDrawRect(renderer, &body);
    SDL_RenderFillRect(renderer, &body);
    SDL_RenderDrawRect(renderer, &tip);
    SDL_RenderFillRect(renderer, &tip);
    SDL_RenderDrawRect(renderer, &tiptip);
    SDL_RenderFillRect(renderer, &tiptip);
}

void Player::move()
{
    if ((x>(SCREEN_WIDTH-width)&&(speed>0)) || ((x<=0) && speed <0))
        speed = 0;
    x += speed;
}

void Player::shoot()
{
    // i dont like this
    auto now = std::chrono::system_clock::now();;
    long current_time_ms = (time_point_cast<std::chrono::milliseconds>(now)).time_since_epoch().count();

    printf("current_time_ms %ld \n", current_time_ms);
    static long last_fired_ms = 0;
    if ((last_fired_ms+500)<current_time_ms) // every 500 ms you can fire
    {
        last_fired_ms = current_time_ms;
        PlayerBullet* pb = new PlayerBullet(x+(width/2),y-height);
    }
}
