#ifndef PLAYER_H_
#define PLAYER_H_

#include <chrono>

#include <SDL2/SDL.h>
#include <SDL2/SDL_rect.h>

#include "entity.h"
#include "player_bullet.h"

extern SDL_Renderer* renderer;

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

class Player : Entity
{
public:
    Player()
    {
        name = "player";
    }
    void draw();
    void move();
    void shoot();

    int8_t speed = 0;
    uint8_t height = 16;
    uint8_t width  = 32;
    uint16_t x = SCREEN_WIDTH/2-width/2;
    uint16_t y = SCREEN_HEIGHT - (16 + height);
};

#endif // PLAYER_H_
