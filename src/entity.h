#ifndef ENTITY_H_
#define ENTITY_H_

#include <stdlib.h> // NULL
#include <stdint.h> // uint8_t
#include <stdio.h>  // printf

class Entity
{
    public:
    Entity();
    virtual ~Entity();
    virtual void draw() = 0;
    virtual void move() = 0;

    uint16_t id;
    char* name;
};
extern Entity* list_of_entities[100];

#endif // ENTITY_H_
