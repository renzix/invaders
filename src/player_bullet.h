#ifndef PLAYER_BULLET_H_
#define PLAYER_BULLET_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_rect.h>

#include "entity.h"
#include "alien.h"

extern SDL_Renderer* renderer;

class PlayerBullet : Entity
{
    public:
    PlayerBullet(uint16_t x, uint16_t y);
    ~PlayerBullet();
    void draw();
    void move();
    const uint8_t height = 10;
    const uint8_t width  = 2;
    uint16_t x;
    uint16_t y;
};

#endif // PLAYER_BULLET_H_
